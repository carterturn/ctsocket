/*
  Copyright 2018 Carter Turnbaugh

  This file is part of Terca C++ Sockets.

  Terca C++ Sockets is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Terca C++ Sockets is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Terca C++ Sockets.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ctserversecure.h"
#include <iostream>
#include <chrono>
#include <thread>

using namespace std;
using namespace std::chrono;
using std::this_thread::sleep_for;

string test_message = "The quick brown fox jumps over the lazy dog. 1 + 2 / 3 + 4 % # \\ \nI am the walrus";

int main(){
	ctserversecure server;
	server.set_key("asdfghjkasdfghjk");

	server.start(5557);
	for(int clients = 0; clients < 3; ){
		ctsocketsecure * new_client = server.try_accept_connection();

		if(new_client != NULL){
			cout << "Got new client\n";

			cout << new_client->c_read() << "\n";
			new_client->c_write(test_message);

			cout << "##########################################################\n";
			cout << "##########################################################\n";
			cout << "##########################################################\n";

			cout << new_client->s_read() << "\n";
			new_client->s_write(test_message);

			new_client->c_close();

			delete new_client;

			clients++;
		}
		else{
			cout << "Waiting for connection...\n";
			sleep_for(seconds(2));
		}
	}

	return 0;
}
