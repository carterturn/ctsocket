/*
  Copyright 2018 Carter Turnbaugh

  This file is part of Terca C++ Sockets.

  Terca C++ Sockets is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Terca C++ Sockets is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Terca C++ Sockets.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ctclient.h"

#include <arpa/inet.h>
#include <resolv.h>
#include <string>
#include <sys/socket.h>
#include <unistd.h>

using std::string;

int connect_socket(string ip, int port){
	int socketid;
	struct sockaddr_in addr;

	if((socketid = socket(AF_INET,SOCK_STREAM,0))<0){
		return -1;
	}

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	inet_aton(ip.c_str(), &addr.sin_addr);

	if(connect(socketid, (sockaddr*)&addr, sizeof(addr)) != 0){
		return -1;
	}

	return socketid;
}

ctsocket * create_client(string ip, int port){
	int socketid = connect_socket(ip, port);

	if(socketid > 0){
		return new ctsocket(socketid);
	}
	return NULL;
}
