/*
  Copyright 2018 Carter Turnbaugh

  This file is part of Terca C++ Sockets.

  Terca C++ Sockets is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Terca C++ Sockets is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Terca C++ Sockets.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ctsocket.h"

#include <chrono>
#include <cstring>
#include <sys/socket.h>
#include <thread>
#include <unistd.h>

using std::string;
using std::chrono::microseconds;
using std::this_thread::sleep_for;

string escape_string(string data){
	string escaped = "";
	for(int i = 0; i < data.size(); i++){
		switch(data[i]){
		case 0:
			escaped += "\\0";
			break;
		case '\n':
			escaped += "\\n";
			break;
		case '\\':
			escaped += "\\\\";
			break;
		default:
			escaped += data[i];
			break;
		}
	}
	return escaped;
}

string unescape_string(string data){
	string unescaped = "";
	for(int i = 0; i < data.size(); i++){
		switch(data[i]){
		case '\\':
			i++;
			switch(data[i]){
			case '0':
				unescaped += '\0';
				break;
			case 'n':
				unescaped += '\n';
				break;
			case '\\':
				unescaped += '\\';
				break;
			default:
				break;
			}
			break;
		default:
			unescaped += data[i];
			break;
		}
	}
	return unescaped;
}

ctsocket::ctsocket(int socketid) : m_socketid(socketid), m_buffer(""), m_is_open(true) {}

string ctsocket::c_read(int timeout_us){
	string data = try_c_read();
	bool do_timeout = timeout_us != 0;
	while(data == "" && (timeout_us > 0 || !do_timeout)){
		data = try_c_read();
		sleep_for(microseconds(10)); // Avoid 100% CPU usage
		timeout_us -= 10;
	}
	return data;
}

string ctsocket::try_c_read(){
	char buffer[256];
	bzero(buffer, 256);
	int bytes_read = 0;
	while((bytes_read = recv(m_socketid, buffer, 256, MSG_DONTWAIT)) > 0){
		for(int i = 0; i < bytes_read; i++){
			m_buffer += buffer[i];
		}
	}
	if(bytes_read < 0){
		if(bytes_read == EBADF){
			m_is_open = false;
		}
	}

	size_t line_ending = m_buffer.find("\n");
	if(line_ending == string::npos){
		return "";
	}

	string data = m_buffer.substr(0, line_ending);
	m_buffer.erase(0, line_ending+1);

	return unescape_string(data);
}

int ctsocket::c_write(string data){
	string escaped_data = escape_string(data);
	escaped_data += "\n";

	int status = write(m_socketid, escaped_data.c_str(), escaped_data.length());
	if(status < 0){
		if(status == EBADF){
			m_is_open = false;
		}
	}
	return status;
}

bool ctsocket::is_open(){
	return m_is_open;
}

int ctsocket::c_close(){
	close(m_socketid);

	return 0;
}
