/*
  Copyright 2018 Carter Turnbaugh

  This file is part of Terca C++ Sockets.

  Terca C++ Sockets is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Terca C++ Sockets is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Terca C++ Sockets.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ctsocketsecure.h"

#include <chrono>
#include <cstring> // bzero
#include <thread>

using std::string;
using std::chrono::microseconds;
using std::this_thread::sleep_for;

ctsocketsecure::ctsocketsecure(int socketid, string key_string) : ctsocket(socketid){
	m_key.data = (unsigned char *) key_string.c_str();
	m_key.size = key_string.length();

	gnutls_cipher_init(&m_cipher_handle, GNUTLS_CIPHER_AES_256_CBC, &m_key, NULL);
}

ctsocketsecure::~ctsocketsecure(){
	gnutls_cipher_deinit(m_cipher_handle);
}

int ctsocketsecure::s_write(string data){
	return c_write(encrypt(data));
}

string ctsocketsecure::s_read(int timeout_us){
	string data = try_s_read();
	bool do_timeout = timeout_us != 0;
	while(data == "" && (timeout_us > 0 || !do_timeout)){
		data = try_s_read();
		sleep_for(microseconds(10));
		timeout_us -= 10;
	}
	return data;
}

string ctsocketsecure::try_s_read(){
	string encrypted_data = try_c_read();
	if(encrypted_data != ""){
		return decrypt(encrypted_data);
	}
	return "";
}

string ctsocketsecure::decrypt(string data){
	char iv[16];
	memcpy(iv, data.c_str(), 16);
	data.erase(0, 16);
	
	unsigned char plaintext[data.length()];
	bzero(plaintext, data.length());

	gnutls_cipher_set_iv(m_cipher_handle, iv, 16);

	gnutls_cipher_decrypt2(m_cipher_handle, data.c_str(), data.length(), plaintext, data.length());

	string result = "";
	for(int i = 0; i < data.length(); i++){
		result += plaintext[i];
	}

	size_t padding_start = result.find_last_not_of('\4');
	if(padding_start == string::npos){
		return result;
	}
	return result.substr(0, padding_start+1);
}

string ctsocketsecure::encrypt(string data){
	int pad_length = 16 - data.length() % 16;
	if(pad_length != 16){
		for(int i = 0; i < pad_length; i++){
			data += '\4';
		}
	}

	unsigned char ciphertext[data.length()];

	char iv[16];
	gnutls_rnd(GNUTLS_RND_NONCE, iv, 16);

	gnutls_cipher_set_iv(m_cipher_handle, iv, 16);

	gnutls_cipher_encrypt2(m_cipher_handle, data.c_str(), data.length(), ciphertext, data.length());

	string result = "";
	for(int i = 0; i < 16; i++){
		result += iv[i];
	}
	for(int i = 0; i < data.length(); i++){
		result += ciphertext[i];
	}

	return result;
}

