/*
   Copyright 2018 Carter Turnbaugh

   This file is part of Terca C++ Sockets.

   Terca C++ Sockets is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Terca C++ Sockets is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Terca C++ Sockets.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <string>

class ctsocket {
public:
	ctsocket(int socketid);

	int c_write(std::string data);

	std::string c_read(int timeout_us = 0);
	std::string try_c_read();

	bool is_open();
	int c_close();

protected:
	int m_socketid;
	std::string m_buffer;
	bool m_is_open;
};
