/*
  Copyright 2018 Carter Turnbaugh

  This file is part of Terca C++ Sockets.

  Terca C++ Sockets is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Terca C++ Sockets is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Terca C++ Sockets.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ctclientsecure.h"
#include <iostream>

using namespace std;

string test_message = "Sockets are good! Sockets are the best! Sockets are good! Sockets are the best! Sockets are good! Sockets are the best! Sockets are good! Sockets are the best! Sockets are good! Sockets are the best! Sockets are good! Sockets are the best!";

int main(){

	ctsocketsecure * client = create_secureclient("127.0.0.1", 5557, "asdfghjkasdfghjk");

	if(client == NULL){
		return -1;
	}

	client->c_write(test_message);
	string server_message = "";
	while((server_message = client->c_read()) == ""){}
	cout << server_message << "\n";

	cout << "##########################################################\n";
	cout << "##########################################################\n";
	cout << "##########################################################\n";

	client->s_write(test_message);
	while((server_message = client->s_read()) == ""){}
	cout << server_message << "\n";

	client->c_close();

	delete client;

	return 0;
}
