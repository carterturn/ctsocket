/*
  Copyright 2018 Carter Turnbaugh

  This file is part of Terca C++ Sockets.

  Terca C++ Sockets is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Terca C++ Sockets is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Terca C++ Sockets.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <arpa/inet.h>
#include <resolv.h>
#include <string>
#include <sys/socket.h>
#include <unistd.h>

#include "ctserver.h"

using std::string;

int ctserver::start(int port){
	m_serversocketid = socket(AF_INET, SOCK_STREAM, 0);

	sockaddr_in server_addr;
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);
	server_addr.sin_addr.s_addr = INADDR_ANY;

	bind(m_serversocketid, (struct sockaddr*) &server_addr, sizeof(struct sockaddr));
	listen(m_serversocketid, 4);

	return 0;
}

ctsocket * ctserver::accept_connection(){
	int socketid = accept(m_serversocketid, NULL, 0);

	if(socketid > 0){
		return new ctsocket(socketid);
	}
	return NULL;
}

ctsocket * ctserver::try_accept_connection(){
	fd_set serversocket_set;
	FD_ZERO(&serversocket_set);
	FD_SET(m_serversocketid, &serversocket_set);
	timeval select_timeout;
	select_timeout.tv_sec = 0;
	select_timeout.tv_usec = 1000;
	if(select(m_serversocketid+1, &serversocket_set, NULL, NULL, &select_timeout) > 0){
		return accept_connection();
	}

	return NULL;
}
