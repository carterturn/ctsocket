/*
  Copyright 2018 Carter Turnbaugh

  This file is part of Terca C++ Sockets.

  Terca C++ Sockets is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Terca C++ Sockets is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Terca C++ Sockets.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <arpa/inet.h>
#include <resolv.h>
#include <string>
#include <sys/socket.h>
#include <unistd.h>

#include "ctclient.h"
#include "ctclientsecure.h"
#include "ctsocketsecure.h"

using std::string;

ctsocketsecure * create_secureclient(int socketid, string key){
	if(socketid > 0){
		return new ctsocketsecure(socketid, key);
	}
	return NULL;
}

ctsocketsecure * create_secureclient(string ip, int port, string key){
	return create_secureclient(connect_socket(ip, port), key);
}
